'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')
    // 116.204.133.231
module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    BASE_API: '"http://47.104.109.132:8083"'
        // BASE_API: '"http://127.0.0.1:8083"'
        // BASE_API: '"http://47.99.104.62:8083"'
})