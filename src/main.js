// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui';
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'
import vuex from 'vuex'
import router from './router'
import $ from 'jquery'
import './utils/element.js'
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import './utils/public.css'
import { default as api } from './api'
Vue.config.productionTip = false
Vue.prototype.api = api
Vue.use(vuex)
Vue.use(VueAxios, axios)
Vue.use(ElementUI);
//引入移动端vue库
import Vant from 'vant';
import 'vant/lib/index.css';
Vue.use(Vant);

Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
// //防止页面直接跳转
router.beforeEach((to, from, next) => {
  if (to.meta.showIndex) {
    next({ path: '/login' })
  } else {
    next({})
  }
})
