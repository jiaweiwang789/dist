/* eslint-disable */

import Mock from 'mockjs';

const data1 = {
  "msg": "请求成功",
  "code": 200,
  "data": [
    {
      "id": 6871,
      "seasonId": 9285,
      "matchId": 113,
      "stageId": 0,
      "matchName": "意女甲",
      "type": 0,
      "position": 1,
      "pts": 34,
      "played": 12,
      "won": 11,
      "drawn": 1,
      "lost": 0,
      "goals": 37,
      "awayGoals": 26,
      "against": 8,
      "diff": 29,
      "teamId": 43203,
      "updateTime": "2020-01-20T19:08:53.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 3631,
      "seasonId": 9209,
      "matchId": 727,
      "stageId": 0,
      "matchName": "阿尔甲",
      "type": 0,
      "position": 1,
      "pts": 29,
      "played": 15,
      "won": 8,
      "drawn": 5,
      "lost": 2,
      "goals": 19,
      "awayGoals": 9,
      "against": 11,
      "diff": 8,
      "teamId": 12362,
      "updateTime": "2020-01-20T19:07:21.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 1117,
      "seasonId": 9310,
      "matchId": 730,
      "stageId": 0,
      "matchName": "埃及超",
      "type": 0,
      "position": 1,
      "pts": 36,
      "played": 12,
      "won": 12,
      "drawn": 0,
      "lost": 0,
      "goals": 35,
      "awayGoals": 18,
      "against": 2,
      "diff": 33,
      "teamId": 14525,
      "updateTime": "2020-01-20T13:51:56.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 7087,
      "seasonId": 9228,
      "matchId": 625,
      "stageId": 0,
      "matchName": "伊朗甲",
      "type": 0,
      "position": 1,
      "pts": 39,
      "played": 20,
      "won": 11,
      "drawn": 6,
      "lost": 3,
      "goals": 32,
      "awayGoals": 13,
      "against": 16,
      "diff": 16,
      "teamId": 22104,
      "updateTime": "2020-01-20T13:51:43.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 3133,
      "seasonId": 9377,
      "matchId": 683,
      "stageId": 0,
      "matchName": "印度甲",
      "type": 0,
      "position": 4,
      "pts": 10,
      "played": 6,
      "won": 3,
      "drawn": 1,
      "lost": 2,
      "goals": 12,
      "awayGoals": 6,
      "against": 8,
      "diff": 4,
      "teamId": 25084,
      "updateTime": "2020-01-20T06:23:27.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 3166,
      "seasonId": 9291,
      "matchId": 664,
      "stageId": 0,
      "matchName": "科威特联",
      "type": 0,
      "position": 2,
      "pts": 17,
      "played": 8,
      "won": 5,
      "drawn": 2,
      "lost": 1,
      "goals": 22,
      "awayGoals": 7,
      "against": 7,
      "diff": 15,
      "teamId": 20896,
      "updateTime": "2020-01-19T21:58:43.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 6685,
      "seasonId": 9021,
      "matchId": 82,
      "stageId": 0,
      "matchName": "英超",
      "type": 0,
      "position": 1,
      "pts": 61,
      "played": 21,
      "won": 20,
      "drawn": 1,
      "lost": 0,
      "goals": 50,
      "awayGoals": 21,
      "against": 14,
      "diff": 36,
      "teamId": 10249,
      "updateTime": "2020-01-18T21:45:13.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 6595,
      "seasonId": 9168,
      "matchId": 108,
      "stageId": 0,
      "matchName": "意甲", "type": 0,
      "position": 1,
      "pts": 48,
      "played": 19,
      "won": 15,
      "drawn": 3,
      "lost": 1,
      "goals": 37,
      "awayGoals": 15,
      "against": 18,
      "diff": 19,
      "teamId": 10467,
      "updateTime": "2020-01-18T21:32:59.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 4342,
      "seasonId": 9265,
      "matchId": 590,
      "stageId": 0,
      "matchName": "澳超",
      "type": 0,
      "position": 1,
      "pts": 34,
      "played": 13,
      "won": 11,
      "drawn": 1,
      "lost": 1,
      "goals": 30,
      "awayGoals": 12,
      "against": 13,
      "diff": 17,
      "teamId": 12976,
      "updateTime": "2020-01-18T19:28:04.000+0000",
      "season": "2019-2020"
    },
    {
      "id": 6406,
      "seasonId": 9243,
      "matchId": 348,
      "stageId": 0,
      "matchName": "以甲",
      "type": 0,
      "position": 1,
      "pts": 41,
      "played": 19,
      "won": 12,
      "drawn": 5,
      "lost": 2,
      "goals": 41,
      "awayGoals": 13,
      "against": 20,
      "diff": 21,
      "teamId": 10266,
      "updateTime": "2020-01-16T17:42:30.000+0000",
      "season": "2019-2020"
    }
  ]
};

const data2 = {
  "msg": "请求成功",
  "code": 200,
  "data": [
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 1,
      "pts": 31,
      "played": 14,
      "won": 9,
      "drawn": 4,
      "lost": 1,
      "goals": 19,
      "awayGoals": 7,
      "against": 8,
      "diff": 11,
      "teamId": 19526,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 2,
      "pts": 25,
      "played": 14,
      "won": 7,
      "drawn": 4,
      "lost": 3,
      "goals": 17,
      "awayGoals": 5,
      "against": 8,
      "diff": 9,
      "teamId": 11846,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 3,
      "pts": 25,
      "played": 14,
      "won": 7,
      "drawn": 4,
      "lost": 3,
      "goals": 15,
      "awayGoals": 7,
      "against": 8,
      "diff": 7,
      "teamId": 13189,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 4,
      "pts": 23,
      "played": 14,
      "won": 6,
      "drawn": 5,
      "lost": 3,
      "goals": 17,
      "awayGoals": 4,
      "against": 10,
      "diff": 7,
      "teamId": 19764,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 5,
      "pts": 23,
      "played": 14,
      "won": 6,
      "drawn": 5,
      "lost": 3,
      "goals": 19,
      "awayGoals": 6,
      "against": 15,
      "diff": 4,
      "teamId": 14147,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 7,
      "pts": 21,
      "played": 13,
      "won": 6,
      "drawn": 3,
      "lost": 4,
      "goals": 13,
      "awayGoals": 7,
      "against": 14,
      "diff": -1,
      "teamId": 23137,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 6,
      "pts": 21,
      "played": 14,
      "won": 6,
      "drawn": 3,
      "lost": 5,
      "goals": 16,
      "awayGoals": 3,
      "against": 13,
      "diff": 3,
      "teamId": 15857,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 8,
      "pts": 19,
      "played": 14,
      "won": 4,
      "drawn": 7,
      "lost": 3,
      "goals": 17,
      "awayGoals": 9,
      "against": 10,
      "diff": 7,
      "teamId": 25681,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 10,
      "pts": 14,
      "played": 14,
      "won": 3,
      "drawn": 5,
      "lost": 6,
      "goals": 13,
      "awayGoals": 4,
      "against": 22,
      "diff": -9,
      "teamId": 22421,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 12,
      "pts": 12,
      "played": 14,
      "won": 3,
      "drawn": 3,
      "lost": 8,
      "goals": 11,
      "awayGoals": 6,
      "against": 22,
      "diff": -11,
      "teamId": 38973,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 14,
      "pts": 11,
      "played": 14,
      "won": 3,
      "drawn": 2,
      "lost": 9,
      "goals": 13,
      "awayGoals": 5,
      "against": 21,
      "diff": -8,
      "teamId": 38985,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 0,
      "position": 13,
      "pts": 11,
      "played": 14,
      "won": 3,
      "drawn": 2,
      "lost": 9,
      "goals": 13,
      "awayGoals": 5,
      "against": 20,
      "diff": -7,
      "teamId": 14299,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 1,
      "position": 14,
      "pts": 7,
      "played": 7,
      "won": 2,
      "drawn": 1,
      "lost": 4,
      "goals": 5,
      "awayGoals": 5,
      "against": 8,
      "diff": -3,
      "teamId": 10924,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    },
    {
      "id": null,
      "seasonId": 9330,
      "matchId": 3021,
      "stageId": 0,
      "matchName": "希腊乙",
      "type": 2,
      "position": 14,
      "pts": 0,
      "played": 6,
      "won": 0,
      "drawn": 0,
      "lost": 6,
      "goals": 0,
      "awayGoals": 0,
      "against": 10,
      "diff": -10,
      "teamId": 37922,
      "updateTime": "2020-01-07T11:54:37.000+0000",
      "season": "2019-2020"
    }
  ]
};

// Mock.mock('http://47.105.174.225:8083/football/getFootballTeamMatchingName', 'post', data1);
// Mock.mock('http://47.105.174.225:8083/football/getFootballTeamMatchScore', 'post', data2);

export default Mock
