const getters = {
  nickname: state => state.user.nickname,
  isBan: state => state.user.isBan,
  uGrade: state => state.user.uGrade,
  uId: state => state.user.uId,
  uSole: state => state.user.uSole,
  utoken: state => state.user.utoken,
  headUrl: state => state.user.headUrl,
  integral: state => state.user.integral
}
export default getters
