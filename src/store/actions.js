
import Cookies from 'js-cookie'
const actions = {
    setPageIndex({ commit, state }, key) {
        Cookies.set('activeIndex', key);
        const pageindex=Cookies.get('activeIndex');
        commit('SET_PAGE', pageindex)
    },
}
export default actions
