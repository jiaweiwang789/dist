import {
  getToken,
  removeToken,
  setToken,
  setLevel,
  removeLevel,
  setUserId,
  removeUserId,
  removeGetIndex,
  setGetIndex,
  setGetIndex2,
  setUserName
} from '@/utils/auth'
import Vue from 'vue'
import Cookies from 'js-cookie'
import { default as api } from '../../api'
import { getLoginPass } from '../../api/api'
import store from '../../store'
import router from '../../router'

const user = {
  state: {
    nickname: '',
    isBan: '',
    uGrade: '',
    uId: '',
    uSole: '',
    utoken: '',
    headUrl1: '',
    integral: ''
  },
  mutations: {
    SET_USER: (state, userInfo) => {
      console.log(userInfo,'===>')
      state.utoken = userInfo.uToken;
      state.uSole = userInfo.uSole;
      state.uId = userInfo.uId;
      state.uGrade = userInfo.uGrade;
      state.isBan = userInfo.isBan;
      state.nickName = userInfo.nickName;
      state.headUrl1 = userInfo.headUrl;
      state.integral = userInfo.integral;
    },
    RESET_USER: (state) => {
      state.utoken = '';
      state.uSole = '';
      state.uId = '';
      state.uGrade = '';
      state.isBan = '';
      state.nickName = '';
      state.headUrl1 = '';
      state.integral = '';
    },
    SET_HEAR: (state, infor) => {
      Vue.set(state, 'nickName', infor.nickName);
      Vue.set(state, 'headUrl1', infor.headUrl)
      console.log(state.nickName)
    }
  },
  actions: {
    // 密码登录
    Login({ commit, state }, loginForm) {
      return new Promise((resolve, reject) => {
        getLoginPass({
          type: 1,
          uPhone: loginForm.uPhone,
          uPwd: loginForm.uPwd
        }).then(res => {
          // console.log(res,'21312312');
          // cookie中保存前端登录状态
          localStorage.setItem("utoken", res.data.uToken);
          Cookies.set('uSole', res.data.uSole);
          Cookies.set('uId', res.data.uId);
          Cookies.set('uGrade', res.data.uGrade);
          Cookies.set('isBan', res.data.isBan);
          Cookies.set('nickName', res.data.nickName);
          Cookies.set('headUrl1', res.data.headUrl);
          Cookies.set('integral', res.data.integral);
          commit('SET_USER', res.data)
          resolve(res);
        }).catch(err => {
          reject(err)
        })
      })
    },
    // 短信登录
    LoginSms({ commit, state }, loginForm) {
      return new Promise((resolve, reject) => {
        getLoginPass({
          type: 2,
          uPhone: loginForm.uPhone,
          code: loginForm.code
        }).then(res => {
          console.log(res);
          // cookie中保存前端登录状态
          localStorage.setItem("utoken", res.data.uToken);
          Cookies.set('uSole', res.data.uSole);
          Cookies.set('uId', res.data.uId);
          Cookies.set('uGrade', res.data.uGrade);
          Cookies.set('isBan', res.data.isBan);
          Cookies.set('nickName', res.data.nickName);
          Cookies.set('headUrl1', res.data.headUrl);
          Cookies.set('integral', res.data.integral);
          commit('SET_USER', res.data)
          resolve(res);
        }).catch(err => {
          reject(err)
        })
      })
    },


    // 修改头像信息
    ModifyInfor({ commit, state }, infor) {
      commit('SET_HEAR', infor)
    },
    // 登出
    LogOut({ commit }) {
      return new Promise((resolve) => {
        localStorage.removeItem("utoken");
        Cookies.remove('uSole');
        Cookies.remove('uId');
        Cookies.remove('uGrade');
        Cookies.remove('isBan');
        Cookies.remove('nickName');
        Cookies.remove('headUrl1');
        Cookies.remove('integral');
        commit('RESET_USER')
        resolve('退出成功');
      })
    },
    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        commit('RESET_USER')
        removeToken()
        resolve()
      })
    }
  }
}
export default user
