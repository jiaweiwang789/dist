import Vue from 'vue'
import Router from 'vue-router'
import routes from './route'

const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}
// import {setTitle} from "../lib/util";
Vue.use(Router);

const router =  new Router({
  // mode:'history',
  linkActiveClass:'router-link-active',
  routes
})

// const HAS_LOGINED = true;
const HAS_LOGINED = window.sessionStorage.getItem('phone') ;
// router.beforeEach((to, from, next) => {
//   // store.dispatch('increment')
//   to.meta && setTitle(to.meta.title)
//   if (to.name!=='login'){
//     if(HAS_LOGINED) next()
//     else next({name:'login'})
//   }else{
//     if(HAS_LOGINED) next({name:'home'})
//     else next()
//   }
// })
// beforeRouteLeave(to, from, next) {
//   console.log('sssss')
//   next()
// }

export default router
