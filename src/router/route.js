var H = window.sessionStorage.getItem('key')
var s = 1
export default [{
    path: '/',
    name: 'home',
    redirect: "/homepage",
    meta: {
        title: '首页',
        showIndex: true
    },
    component: () =>
        import ('../views/home'),
    children: [{
            path: '/login',
            name: 'login',
            meta: {
                title: '登录',
                showIndex: false
            },
            component: () =>
                import ('../views/login')
        }, {
            path: '/registered',
            name: 'registered',
            meta: {
                title: '注册',
                showIndex: false
            },
            component: () =>
                import ('../views/login/registered.vue')
        },
        {
            path: '/forgetPassword',
            name: 'forgetPassword',
            meta: {
                title: '注册',
                showIndex: false
            },
            component: () =>
                import ('../views/login/forgetPassword.vue')
        },
        {
            path: '/homepage',
            name: 'homepage',
            redirect: "/dataPage",
            meta: {
                title: '首页版块',
                showIndex: false
            },
            component: () =>
                import ('../views/homePage'),
            children: [
                /*弃用{
                  path: '/defaultContent',
                  name: 'defaultContent',
                  meta: {
                    title: '首页版块',
                    showIndex: false
                  },
                  component: () => import('../views/homePage/defaultContent_弃用.vue')
                },*/
                {
                    path: '/dataPage',
                    name: 'dataPage',
                    meta: {
                        title: '数据首页',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/dataPage.vue')
                },
                {
                    path: '/dataKinds',
                    name: 'dataKinds',
                    meta: {
                        title: '赛事数据',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/dataKinds.vue')
                },
                {
                    path: '/esports',
                    name: 'esports',
                    meta: {
                        title: '电竞版块',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/eSports.vue')
                },
                {
                    path: '/football',
                    name: 'football',
                    meta: {
                        title: '足球版块',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/footBall.vue')
                },
                {
                    path: '/search',
                    name: 'search',
                    meta: {
                        title: '搜索版块',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/search.vue')
                },
                {
                    path: '/vis',
                    name: 'vis',
                    meta: {
                        title: '用户协议',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/vis.vue')
                },
                {
                    path: '/vis',
                    name: 'vis',
                    meta: {
                        title: '用户协议',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/homePage/vis.vue')
                }
            ]
        },
        {
            path: '/gamedetailsfootball',
            name: 'gamedetailsfootball',
            meta: {
                title: '足球比赛详情',
                showIndex: false
            },
            component: () =>
                import ('../views/gamesDetails/gameDeatailsFootball.vue')
        },
        {
            path: '/gamedetailsbasketball',
            name: 'gamedetailsbasketball',
            meta: {
                title: '篮球比赛详情',
                showIndex: false
            },
            component: () =>
                import ('../views/gamesDetails/gameDeatailsBasketball.vue')
        },
        {
            path: '/gamedetailsesports',
            name: 'gamedetailsesports',
            meta: {
                title: '电竞比赛详情',
                showIndex: false
            },
            component: () =>
                import ('../views/gamesDetails/gameDetailsEsports.vue')
        },
        {
            path: '/godindex',
            name: 'godindex',
            meta: {
                title: '大神推荐版块',
                showIndex: false
            },
            component: () =>
                import ('../views/godRecommended')
        },
        {
            path: '/godindex/godIndexDetail',
            name: 'godIndexDetail',
            meta: {
                title: '大神推荐详情页面',
                showIndex: false
            },
            component: () =>
                import ('../views/godRecommended/godIndexDetail')
        },
        {
            path: '/goddetails',
            name: 'goddetails',
            meta: {
                title: '大神推荐详情',
                showIndex: false
            },
            component: () =>
                import ('../views/godRecommended/godDetails.vue')
        },
        {
            path: '/redpersond',
            name: 'redpersond',
            meta: {
                title: '红人榜详情',
                showIndex: false
            },
            component: () =>
                import ('../views/godRecommended/redPersonD.vue')
        },
        {
            path: '/bbs',
            name: 'bbs',
            meta: {
                title: '论坛版块',
                showIndex: false,
            },
            component: () =>
                import ('../views/bbs'),
        },

        {
            path: 'bbs/bbsdetails',
            name: 'bbsdetails',
            meta: {
                title: '论坛版块详情',
                showIndex: false
            },
            component: () =>
                import ('../views/bbs/bbsDetails.vue'),
        },
        {
            path: '/activity',
            name: 'activity',
            meta: {
                title: '活动版块',
                showIndex: false
            },
            component: () =>
                import ('../views/activity'),
        },
        {
            path: '/actdetails',
            name: 'actdetails',
            meta: {
                title: '活动详情版块',
                showIndex: false
            },
            component: () =>
                import ('../views/activity/actDetails.vue'),
        },
        {
            path: '/personal',
            name: 'personal',
            redirect: "/attentiontopic",
            meta: {
                title: '个人中心',
                showIndex: false
            },
            component: () =>
                import ('../views/personal'),
            children: [{
                    path: '/attentiontopic',
                    name: 'attentiontopic',
                    meta: {
                        title: '我关注的话题',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/attentionTopic.vue')
                },
                {
                    path: '/subjectparticipate',
                    name: 'subjectparticipate',
                    meta: {
                        title: '我参与的话题',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/subjectParticipate.vue')
                },
                {
                    path: '/releasetopic',
                    name: 'releasetopic',
                    meta: {
                        title: '我发布的话题',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/releaseTopic.vue')
                },
                {
                    path: '/focusgod',
                    name: 'focusgod',
                    meta: {
                        title: '我关注的大神',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/focusGod.vue')
                },
                {
                    path: '/integralhistory',
                    name: 'integralhistory',
                    meta: {
                        title: '积分历史记录',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/integralHistory.vue')
                },
                {
                    path: '/myfans',
                    name: 'myfans',
                    meta: {
                        title: '我的粉丝',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/myFans.vue')
                },
                {
                    path: '/myquiz',
                    name: 'myquiz',
                    meta: {
                        title: '我的竞猜',
                        showIndex: false
                    },
                    component: () =>
                        import ('../views/personal/myQuiz.vue')
                },

            ]
        }
    ]
}, {
    path: '/release_topic',
    name: 'release_topic',
    meta: {
        title: '发布评论',
        showIndex: false
    },
    component: () =>
        import ('../views/h5/release_topic.vue')
}, {
    path: '/topic_details',
    name: 'topic_details',
    meta: {
        title: '话题详情',
        showIndex: false
    },
    component: () =>
        import ('../views/h5/topic_details.vue')
}, {
    path: '/analysis_details',
    name: 'analysis_details',
    meta: {
        title: '大神详情',
        showIndex: false
    },
    component: () =>
        import ('../views/h5/analysis_details.vue')
}, {
    path: '/gad_release_topic',
    name: 'gad_release_topic',
    meta: {
        title: '大神分析发布',
        showIndex: false
    },
    component: () =>
        import ('../views/h5/gad_release_topic.vue')
}, {
    path: '/user_agreement',
    name: 'user_agreement',
    meta: {
        title: '用户协议',
        showIndex: false
    },
    component: () =>
        import ('../views/h5/user_agreement.vue')
}, {
    path: '/article_details',
    name: 'article_details',
    meta: {
        title: 'test',
        showIndex: false
    },
    component: () =>
        import ('../views/h5/article_details.vue')
}, ]