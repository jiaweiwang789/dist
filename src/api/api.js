import {get, post } from '../api'
// 密码登录
export const getLoginPass = p => post('/user/login', p);

// 获取验证码
export const sendCode = p => post('/user/sendPhoneCode', p);

// 注册用户
export const registerUser = p => post('/user/registerUser', p);
//修改密码
export const updatePwdByCode = p => post('/user/updatePwdByCode', p);


// 获取文章列表
export const getArticleList = p => post('/article/getArticleList', p);

// 获取文章详情
export const getArticleByAid = p => get('/article/getArticleByAid', p);

// 获取评论列表
export const getRemarkList = p => post('/articleOther/getRemarkList', p);

// 上传图片
export const uploadImg = p => post('/upload/img', p);

// 查询联赛下拉框
export const getLeagueList = p => post('/computerMatch/getLeagueList', p);

// 添加文章
export const addArticle = p => post('/article/addArticle', p);

// 喜欢/取消喜欢文章
export const doArticleLike = p => post('/articleOther/doArticleLike', p);

// 添加评论
export const addRemark = p => post('/articleOther/addRemark', p);

//收藏/取消文章
export const doArticleCollection = p => post('/articleOther/doArticleCollection', p);

//修改文章
export const updateArticle = p => post('/article/updateArticle', p);

//查看关注的文章集合
export const getCollectionArticleList = p => post('/article/getCollectionArticleList', p);

//获取我参与的文章
export const getRemarkArticleList = p => post('/article/getRemarkArticleList', p);

//查看我发布的文章
export const getArticleListByUId = p => post('/article/getArticleListByUId', p);

//查看积分详情
export const getAddIntegralRecord = p => post('/addIntegral/getAddIntegralRecord', p);

//删除文章
export const deleteArticleByAId = p => get('/article/deleteArticleByAId', p);

//查询赛事列表
export const getComputerAllStatus = p => post('/computerMatch/getComputerAllStatus', p);
//新增查询电竞根据日期获取全部赛事状态
export const getComputerMatchList = p => post('/computerMatch/getComputerMatchList', p);



//查询足球赛事列表
export const getFootballMatchList = p => post('/football/getFootballAllStatus', p);

//获取足球联赛下拉框列表
export const getFootballMatchOption = p => get('/football/getFootballMatchOption', p);

//篮球赛事列表
export const getBasketballMatchList = p => post('/basketball/getBasketballAllStatus', p);

//获取篮球联赛下拉框列表
export const getBasketballMatchOption = p => get('/basketball/getBasketballMatchOption', p);

//电竞赛事前瞻分析
export const getComputerMatchDetail = p => get('/computerMatch/getComputerMatchDetail', p);

//获取热门赛事列表
export const getHotMatchList = p => get('/backstageHotMatch/getHotMatchList', p);

//查看轮播图列表
export const getCarouseList = p => get('/backstageCarousel/getCarouseList', p);

//足球赛事分析
export const getFootballMatchanAlysis = p => get('/football/getFootballMatchanAlysis', p);

//足球详情之历史记录
export const getTeamMatchHistory = p => post('/football/getTeamMatchHistory', p);

//足球详情之历史交锋
export const getTeamsBattle = p => post('/football/getTeamsBattle', p);

//获取联赛积分
export const getFootballMatchPoints = p => post('/football/getFootballMatchPoints', p);

//获取篮球赛事详情
export const getBasketballMatchAnalysis = p => get('/basketball/getBasketballMatchAnalysis', p);

//篮球获取球队历史记录
export const basketballgetTeamMatchHistory = p => post('/basketball/getTeamMatchHistory', p);

//篮球两队交锋记录
export const basketballgetTeamsBattle = p => post('/basketball/getTeamsBattle', p);

//查看活动列表
export const getActiveList = p => post('/backstageActive/getActiveList', p);

//查看活动详情
export const getActiveById = p => get('/backstageActive/getActiveById', p);

//获取大神列表
export const getGreatGodList = p => post('/user/getGreatGodList', p);

//获取大神分析列表
export const getGreatForecastList = p => post('/greatForecast/getGreatForecastList', p);

//查看大神推荐详情
export const getGreatForecastByGfId = p => get('/greatForecast/getGreatForecastByGfId', p);

//判断是否购买大神分析
export const isBuyGreatForccast = p => get('/greatForecast/isBuyGreatForccast', p);

//购买大神分析
export const buyGreateForecast = p => get('/greatForecast/buyGreateForecast', p);

//下注
export const addMatchBet = p => post('/matchBet/addMatchBet', p);

//个人中心
export const selectUserByUserId = p => get('/user/selectUserByUserId', p);
//个人中心
export const selectUserNotSelf = p => get('/user/selectUserNotSelf', p);

//获取粉丝列表
export const getFansList = p => post('/user/getFansList', p);

//修改用户信息
export const updateUser = p => post('/user/updateUser', p);

//获取我关注的大神列表
export const getMyGreatGodList = p => post('/user/getMyGreatGodList', p);

//获取下注列表
export const getMatchBetList = p => post('/matchBet/getMatchBetList', p);

//关注大神 取消关注
export const doUserFollow = p => get('/user/doUserFollow', p);

//申请成为大神
export const applicationGreatGod = p => get('/user/applicationGreatGod', p);

//获取热门文章
export const getHotArticle = p => get('/article/getHotArticle', p);

//添加大神分析
export const addGreatForecast = p => post('/greatForecast/addGreatForecast', p);

//新增埋点
export const addDetailsLog = p => post('/detailsLog/addDetailsLog', p);

// 修改大神分析
export const updateGreatForecast = p => post('/greatForecast/updateGreatForecast', p);

// 首页：获取精彩推荐文章
export const getHomePageNews = p => get('/article/getHotArticle', p);

// 首页：获取策略分析
export const getHomePagePlans = p => post('/greatForecast/getGreatForecastList', p);

// 首页：获取数据分析-足球战队获取赛季赛事名称
export const getFootballTeamMatchingName = p => post('/football/getFootballTeamMatchingName', p);

// 首页：获取数据分析-足球站队战绩排名列表
export const getFootballTeamMatchScore = p => post('/football/getFootballTeamMatchScore', p);

//首页：获取数据分析-足球战队获取赛季赛事名称
export const getFootballMatchingName = p => post('/football/getFootballMatchingName', p);

//首页：获取数据分析-电竞获取赛季排名列表
export const getComputerTeamInLeagueRank = p => post('/computerMatch/getComputerTeamInLeagueRank', p);

//首页：获取首页-热门推荐
export const getTopArticle = p => post('/article/getTopArticle', p);

//置顶搜索
export const getTopSearchAll = p => post('/topSearch/getTopSearchAll', p);