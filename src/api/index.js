import axios from 'axios'
import QS from 'qs'

import router from '../router'

import { Message, Loading } from 'element-ui';
import Vue from 'vue'
import _ from 'lodash';
let loadingInstance; //loading 实例
let needLoadingRequestCount = 0; //当前正在请求的数量


function showLoading() {
    let main = document.querySelector('#app')
    if (main) {
        if (needLoadingRequestCount === 0 && !loadingInstance) {
            loadingInstance = Loading.service({
                target: main,
                text: '正在加载...',
                background: 'rgba(0,0,0,0.3)'
            });
        }
        needLoadingRequestCount++;
    }
}

function closeLoading() {
    Vue.nextTick(() => { // 以服务的方式调用的 Loading 需要异步关闭
        needLoadingRequestCount--;
        needLoadingRequestCount = Math.max(needLoadingRequestCount, 0); // 保证大于等于0
        if (needLoadingRequestCount === 0) {
            if (loadingInstance) {
                hideLoading()
            }
        }
    });
}
//防抖：将 300ms 间隔内的关闭 loading 便合并为一次。防止连续请求时， loading闪烁的问题。
//因为有时会碰到在一次请求完毕后又立刻又发起一个新的请求的情况（比如删除一个表格一行后立刻进行刷新）
//这种情况会造成连续 loading 两次，并且中间有一次极短的闪烁。通过防抖可以让 300ms 间隔内的 loading 便合并为一次，避免闪烁的情况。
var hideLoading = _.debounce(() => {
    loadingInstance.close();
    loadingInstance = null;
}, 300);


// 创建axios实例
const service = axios.create({
        baseURL: process.env.BASE_API,
        // baseURL:'http://47.111.101.184:8095',
        // baseURL:'http://192.168.1.55:8095',
        timeout: 15000, // 请求超时时间2
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        }
    })
    // request拦截器
service.interceptors.request.use(config => {
        let token = localStorage.getItem("utoken")

        if (token) {
            config.headers.common['token'] = token
        }
        showLoading()



        return config
    }, error => {
        // Do something with request error
        closeLoading()
        console.error(error) // for debug
        Promise.reject(error)
    })
    // respone拦截器
service.interceptors.response.use(
    response => {
        const res = response.data;
        if (res.code == '10030' || res.code == '10031' || res.code == '10032' || res.code == '10033') {
            localStorage.removeItem('utoken');
            // Message({
            //   message: res.msg,
            //   type: 'error',
            //   duration: 3 * 1000
            // })
            router.replace({
                path: '/' // 到登录页重新获取token
            })



            return Promise.reject(res.msg)

        } else if (res.code == '200') {
            closeLoading()
                // console.log('已完成loading')
            return res
        } else {
            return Promise.reject(res.msg)
        }
    },
    error => {
        closeLoading()
        console.log(error) // for debug
    }
)

/**
 * get方法，对应get请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */
export function get(url, params) {
    console.log(params)
    return new Promise((resolve, reject) => {
        service.get(url, {
            params: params
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}
/**
 * post方法，对应post请求
 * @param {String} url [请求的url地址]
 * @param {Object} params [请求时携带的参数]
 */

export function post(url, params) {
    return new Promise((resolve, reject) => {
        service.post(url, params).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}





export default service