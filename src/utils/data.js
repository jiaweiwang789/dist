/**
 * JS获取距当前时间差
 * 
 * @param int time JS毫秒时间戳
 *
 */
export function get_time_diff(time) {
    var diff = '';
    var time_diff = new Date().getTime() - time;
    // 计算相差天数  
    var days = Math.floor(time_diff / (24 * 3600 * 1000));
    // if (days > 0) {
    //     diff += days + '天';
    // }
    // 计算相差小时数  
    var leave1 = time_diff % (24 * 3600 * 1000);
    var hours = Math.floor(leave1 / (3600 * 1000));
    // if (hours > 0) {
    //     diff += hours + '小时';
    // } else {
    //     if (diff !== '') {
    //         diff += hours + '小时';
    //     }
    // }
    // 计算相差分钟数  
    var leave2 = leave1 % (3600 * 1000);
    var minutes = Math.floor(leave2 / (60 * 1000));
    // if (minutes > 0) {
    //     diff += minutes + '分钟';
    // } else {
    //     if (diff !== '') {
    //         diff += minutes + '分钟';
    //     }
    // }
    // // 计算相差秒数  
    var leave3 = leave2 % (60 * 1000);
    var seconds = Math.round(leave3 / 1000);
    // if (seconds > 0) {
    //     diff += seconds + '秒';
    // } else {
    //     if (diff !== '') {
    //         diff += seconds + '秒';
    //     }
    // }

    // var leave3 = leave2 % (60 * 1000);
    // var seconds = Math.round(leave3 / 1000);
    // if (seconds > 0) {
    //     diff += seconds + '秒';
    // }

    if (days > 365) {
        return '1年'
    } else if (days > 180) {
        return '6个月'
    } else if (days > 90) {
        return '3个月'
    } else if (days > 30) {
        return '1个月'
    } else if (days < 30 && days > 0) {
        return days + '天'
    } else if (seconds > 0 && seconds < 60 && minutes <= 1) {
        return '1分钟'
    } else {
        console.log(hours)
        console.log(seconds)
        console.log(minutes)
        console.log(days)
        console.log(11)
        var str = '';
        if (hours > 0) {
            str += hours + '小时';
            if (minutes > 0) {
                str += minutes + '分钟';
            }
        } else {
            if (minutes > 0) {
                str += minutes + '分钟';
            }
        }
        return str
    }



    // return diff;
}




export function get_time_diff2(time) {
    var diff = '';
    var time_diff = new Date().getTime() - time;
    // 计算相差天数 
    var days = Math.floor(time_diff / (24 * 3600 * 1000));
    if (days > 0) {
    diff += days + '天';
    }
    // 计算相差小时数 
    var leave1 = time_diff % ( 24 * 3600 * 1000); 
    var hours = Math.floor(leave1 / (3600 * 1000));
    if (hours > 0) {
    diff += hours + '小时';
    } else {
    if (diff !== '') {
    diff += hours + '小时';
    }
    }
    // 计算相差分钟数 
    var leave2 =leave1 % (3600 * 1000);
    var minutes = Math.floor(leave2 / (60 * 1000));
    if (minutes > 0) {
    diff += minutes + '分';
    } else {
    if (diff !== '') {
    diff += minutes + '分';
    }
    }
    // 计算相差秒数 
    var leave3 = leave2%(60*1000);
    var seconds = Math.round(leave3/1000);
    if (seconds > 0) {
    diff += seconds + '秒';
    } else {
    if (diff !== '') {
    diff += seconds + '秒';
    }
    }
    
    return diff;
   }