import Cookies from 'js-cookie'

const LoginKey = 'hasLogin'

export function getToken() {
  return Cookies.get(LoginKey)
}

export function setToken() {
  return Cookies.set(LoginKey, '1')
}

export function removeToken() {
  return Cookies.remove(LoginKey)
}

export function setLevel(name) {
  return Cookies.set('LoginLevel',name)
}
export function removeLevel() {
  return Cookies.remove('LoginLevel')
}
export function removeUserId() {
  return Cookies.remove('UserId')
}
export function getCookie(o) {
  return Cookies.get(o)
}
export function setUserId(name) {
  return Cookies.set('UserId',name)
}
export function setGetIndex() {
  return Cookies.set('GetIndex','1-1')
}
export function setGetIndex2() {
  return Cookies.set('GetIndex','1-4')
}
export function removeGetIndex() {
  return Cookies.remove('GetIndex')
}
export function setUserName(v) {
  return Cookies.set('UserName1',v)
}
export function getUserName() {
  return Cookies.set('UserName1')
}